# Themed fast 404

Creates themed static 404 page and serves it instead of dynamic response.

## How it works

This module provides dynamic 404 `/page-not-found` page.
For each enabled language, the module's cron job will access the dynamic 404 page, grab its contents, and put it into a static html file.

The module's config factory override service will automatically set the `system.performance.fast_404` config for you.
After that, you have to make sure to run cron in order to generate your first static 404 page.

## Installation

See https://www.drupal.org/docs/extending-drupal/installing-modules
for instructions on how to install or update Drupal modules.

## Configuration page

Module configuration page: `/admin/config/system/themed_fast_404`

* Base URL: On some hosting providers it is impossible to pass parameters
to cron to tell Drupal which URL to bootstrap with. In that case, the base URL
will be invalid/incorrect and Drupal will not be able to generate the static 404 file.
It is recommended to set it.
* 404 page body: The dynamic page not found body content.

## Setup

* Enable the module
* Visit `/admin/config/system/themed_fast_404` and configure it.

### Override the default `fast_404` settings

If there is a need to additionally tweak `fast_404` settings you can do it via `settings.php` file. E.g.:
```php
// Drupal core fast_404 configuration. More info is available in default.settings.php
$config['system.performance']['fast_404']['exclude_paths'] = ...;
```
