<?php

namespace Drupal\themed_fast_404;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Drupal\file\FileRepositoryInterface;

/**
 * Fast 404 manager service.
 */
class ThemedFast404Manager implements ThemedFast404ManagerInterface {

  /**
   * Themed fast 404 constructor.
   */
  public function __construct(
    protected FileRepositoryInterface $fileRepository,
    protected ConfigFactory $configFactory,
    protected LanguageManagerInterface $languageManager,
  ) {}

  /**
   * {@inheritDoc}
   */
  public function buildStatic404() {
    foreach ($this->languageManager->getLanguages() as $language) {
      $lng_id = $language->getId();
      $url = $this->get404Url($language);

      // Get Drupal 404 page contents.
      $html = @file_get_contents($url);
      $destination = static::PAGE_NOT_FOUND_FILE_PATH . 'page-not-found-' . $lng_id . '.html';
      $this->fileRepository->writeData($html, $destination, FileSystemInterface::EXISTS_REPLACE);
    }
  }

  /**
   * Get Static html 404 url.
   */
  public function getStatic404url():string | bool {
    $current_language = $this->languageManager->getCurrentLanguage();
    $uri = ThemedFast404ManagerInterface::PAGE_NOT_FOUND_FILE_PATH . 'page-not-found-' . $current_language->getId() . '.html';

    if (file_exists($uri)) {
      $file = $this->fileRepository->loadByUri($uri);
      $url = $file->createFileUrl(FALSE);

      return $url;
    }

    return FALSE;
  }

  /**
   * Get the Fast 404 url.
   */
  public function get404Url(LanguageInterface $language):string {
    $use_system_404 = $this->configFactory->get('themed_fast_404.settings')->get('use_system_404');
    $base_url = $this->configFactory->get('themed_fast_404.settings')->get('base_url');

    // Set initial route.
    $url = Url::fromRoute(static::DRUPAL_404_ROUTE_NAME);

    // If use_system_404 setting is enabled and system 404 page is set.
    if ($use_system_404 && $system_404_url = $this->configFactory->get('system.site')->get('page')['404']) {
      $url = Url::fromUserInput($system_404_url);
    }
    $url->setOption('language', $language);

    return $base_url ? $base_url . $url->toString() : $url->setAbsolute()->toString();
  }

}
