<?php

namespace Drupal\themed_fast_404\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\themed_fast_404\ThemedFast404ManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide settings for Themed Fast 404.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Constructor.
   */
  public function __construct(
    protected ConfigFactoryInterface $config_factory,
    protected ThemedFast404ManagerInterface $themedFast404Manager,
  ) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('themed_fast_404.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'themed_fast_404_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'themed_fast_404.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $field_type = NULL) {
    $config = $this->config('themed_fast_404.settings');

    if ($static_url = $this->themedFast404Manager->getStatic404url()) {
      $this->messenger()->addMessage($this->t('Static 404 page is available at <a href="@url" target="_blank">@url</a>', [
        '@url' => $static_url,
      ]));
    }

    // Tree element.
    $form['settings'] = [
      '#tree' => TRUE,
    ];

    $form['settings']['use_system_404'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use system 404 path'),
      '#default_value' => $config->get('use_system_404'),
    ];

    $form['settings']['base_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Base URL'),
      '#default_value' => $config->get('base_url'),
      '#required' => FALSE,
      '#description' => $this->t('On some hosting providers it is impossible to pass parameters to cron to tell Drupal which URL to bootstrap with.
        In that case base URL will be invalid/incorrect and drupal will not be able to generate static 404 file.
        In that case the base URL can be set here.<br>Example: <em>@url</em>', ['@url' => $GLOBALS['base_url']]),
    ];

    $use_system_path_disabled = [
      ':input[name="settings[use_system_404]"]' => [
        'checked' => FALSE,
      ],
    ];

    $use_system_path_condition = [
      'visible' => $use_system_path_disabled,
      'required' => $use_system_path_disabled,
    ];

    $form['settings']['404_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('404 page body'),
      '#default_value' => $config->get('404_body'),
      '#states' => $use_system_path_condition,
    ];

    $form['rebuild'] = [
      '#type' => 'submit',
      '#value' => $static_url ? $this->t('Rebuild static 404 page') : $this->t('Build static 404 page'),
      '#submit' => ['::rebuild404'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Rebuild static 404 page.
   */
  public function rebuild404(array &$form, FormStateInterface $form_state) {
    $this->themedFast404Manager->buildStatic404();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('themed_fast_404.settings')
      ->setData($form_state->getValue('settings'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
