<?php

namespace Drupal\themed_fast_404;

/**
 * Provides an interface for ThemedFast404Manager service.
 */
interface ThemedFast404ManagerInterface {

  /**
   * Drupal (dynamic) 404 page route name.
   */
  const DRUPAL_404_ROUTE_NAME = 'themed_fast_404.page_not_found';

  /**
   * Static 404 file path.
   */
  const PAGE_NOT_FOUND_FILE_PATH = 'public://';

  /**
   * Build static 404 page.
   */
  public function buildStatic404();

  /**
   * Get the 404 page url.
   *
   * @return string|bool
   *   The 404 url string
   */
  public function getStatic404Url():string | bool;

}
