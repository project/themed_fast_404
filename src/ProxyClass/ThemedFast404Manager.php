<?php

namespace Drupal\themed_fast_404\ProxyClass;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\themed_fast_404\ThemedFast404ManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a proxy class for \Drupal\themed_fast_404\ThemedFast404Manager.
 *
 * @see \Drupal\Component\ProxyBuilder\ProxyBuilder
 */
class ThemedFast404Manager implements ThemedFast404ManagerInterface {

  use DependencySerializationTrait;

  /**
   * The real proxied service, after it was lazy loaded.
   *
   * @var \Drupal\themed_fast_404\Config\ConfigOverrider
   */
  protected $service;

  /**
   * Constructs a ProxyClass Drupal proxy object.
   */
  public function __construct(
    protected ContainerInterface $container,
    protected $drupalProxyOriginalServiceId,
  ) {}

  /**
   * Lazy loads the real service from the container.
   *
   * @return object
   *   Returns the constructed real service.
   */
  protected function lazyLoadItself() {
    if (!isset($this->service)) {
      $this->service = $this->container->get($this->drupalProxyOriginalServiceId);
    }

    return $this->service;
  }

  /**
   * {@inheritdoc}
   */
  public function buildStatic404() {
    return $this->lazyLoadItself()->buildStatic404();
  }

  /**
   * {@inheritdoc}
   */
  public function getStatic404Url():string | bool {
    return $this->lazyLoadItself()->getStatic404Url();
  }

}
