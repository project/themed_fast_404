<?php

namespace Drupal\themed_fast_404\Config;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\themed_fast_404\ThemedFast404ManagerInterface;

/**
 * Automatically overrides system.performance.fast_404 configuration.
 */
class ConfigOverrider implements ConfigFactoryOverrideInterface {

  /**
   * Construct config override service.
   */
  public function __construct(
    protected FileSystemInterface $file_system,
    protected FileUrlGeneratorInterface $fileUrlGenerator,
    protected ThemedFast404ManagerInterface $themedFast404Manager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    // Validation.
    if (!in_array('system.performance', $names)) {
      return [];
    }

    // Get static 404 page url.
    $url = $this->themedFast404Manager->getStatic404url();
    if ($url) {
      $overrides['system.performance']['fast_404']['html'] = @file_get_contents($url);
    }

    $overrides['system.performance']['fast_404']['exclude_paths'] = '/\/(?:styles)|(?:system\/files)\//';
    $overrides['system.performance']['fast_404']['paths'] = '/\.*$/i';

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return 'ConfigOverrider';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}
